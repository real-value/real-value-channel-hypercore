# Real-Value-Channel-Hypercore Library    

An implemententation of a communication channel using [hypercore](https://www.npmjs.com/package/hypercore).

## How to use

The goal of this functionality is to support the following type of expression
```
model.from([1,2,3]).toChannel()
model.fromChannel().log()
```

## Install

```
npm install real-value-channel-hypercore
```


## Usage
```
//Create a RAM hypercore
const channel = ChannelFactory('ram')

//Create a file system base hypercore
const channel = ChannelFactory('./somedirectory')

//Then you add to the channel using the enqueue method
channel.enqueue({foo: 'bar'})

//Then you read from the channel using the dequeue and length methods
if(channel.length()>0)
    let nextvalue = channel.dequeue()
```
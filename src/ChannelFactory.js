let debug = require('debug')('ChannelHypercore')

let hypercore = require('hypercore')

module.exports = (storage,eventHandler=null)=>{

    let feed = null
    if(storage==='ram'){
        let ram = require('random-access-memory')
        const ramstorage = ()=>ram()
        feed = hypercore(ramstorage,{
            valueEncoding: 'json'
        })
    }   else {
        feed = hypercore(storage,{
            valueEncoding: 'json'
        })
    }

    let stream = feed.createWriteStream()
        
    const inBuffer=[]

    let eHandler = eventHandler?eventHandler : x=>{
        inBuffer.push(x.value)
    }
    feed.createReadStream({live: true}).on('data',eHandler)
    
    function ChannelFactory (options={}) {    
        return {
            enqueue: x => {
                debug(`enqueue`)
                stream.write({value:x})
            },
            length: ()=>inBuffer.length,
            dequeue: () => {
                debug(`dequeue buffer length:${inBuffer.length}`)
                return inBuffer.shift()
            }
        }
        
    }

    return ChannelFactory
    
}
    
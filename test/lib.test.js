import { describe } from 'riteway'
const debug = require('debug')('test')

let ChannelFactory = require('../index.js')

describe('Channel', async (assert) => {

  let f = ChannelFactory('ram')

  const channelFactory = ChannelFactory('ram')
  let channelWriter = channelFactory()
  let channelReader = channelFactory()

  channelWriter.enqueue('1')
  channelWriter.enqueue('2')

  await new Promise((resolve)=>{ setTimeout(resolve,100)})

  let serverItems= []
  if(channelReader.length()>0){
    serverItems.push(channelReader.dequeue())
    serverItems.push(channelReader.dequeue())
  }

  assert({
    given: 'Content sent',
    should: 'content be received',
    actual: `${serverItems}`,
    expected: '1,2'
  })
  
})
